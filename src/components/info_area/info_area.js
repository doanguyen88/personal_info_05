import React, { Component } from 'react';
import './info_area.css';

var FA = require('react-fontawesome')

export const InfoArea = (props) => {
  return (
    <section className={`icon-section ${props.className}`}>
      <div className="icon-wrapper">
        <div className="icon-box">
          <i className={`icon fa ${props.name}`} />
          <div className="icon-text">{props.text}</div>
        </div>
      </div>
    </section>
  );
}

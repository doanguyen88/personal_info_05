import React, { Component } from 'react';
import { InfoArea } from '../../components/info_area/info_area';
import './home.css';

export class Home extends Component {
  render() {
    return (
      <div className="home">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        
        <div className="flex-wrap full-size">
          <InfoArea className="one-third" text="Abount me" name="fa-user" />
          <InfoArea className="one-third" text="Education" name="fa-graduation-cap" />
          <InfoArea className="one-third" text="Work experience" name="fa-briefcase" />
          <InfoArea className="one-third" text="Portfolio" name="fa-archive" />
          <InfoArea className="one-third" text="Blog" name="fa-themeisle" />
          <InfoArea className="one-third" text="Contact" name="fa-location-arrow" />
        </div>
      </div>
    );
  }
}

